function createNewUser() {
    let aFirstName = readField("Your name");
    let aSurname = readField("Your surname");
    let dateAgeUser = readBDay();
    let yearDateAgeUser = dateAgeUser[0];
    let monthDateAgeUser = dateAgeUser[1];
    let dayDateAgeUser = dateAgeUser[2];
    let dateYearNow = new Date().getFullYear();
    let userAge = 0;
    let password = " ";
    let dateMonthNow = new Date().getMonth() - 1;
    let dateDayNow = new Date().getDay();

    return newUser = {

        _firstName: aFirstName,
        get firstName() {
            return this._firstName
        },
        set firstName(value) {
            this._firstName = value;
            console.log(`Name updated ${value}`);
        },
        refreshFirstName() {
            this.firstName = prompt("Refresh your surname");
        },
        _surname: aSurname,
        get surname() {
            return this._surname;
        },
        set surname(value) {
            this._surname = value;
            console.log(`Surname updated ${value}`);
        },
        refreshSurname() {
            this.surname = prompt("Refresh your surname")
        },
        getLogin() {
            return (this.firstName[0] + this.surname).toLowerCase();
        },

         _birthday: yearDateAgeUser,
        get birthday(){
            return this._birthday
        },
        get getAge(){
            if(dateMonthNow < monthDateAgeUser){
                return dateYearNow - this.birthday - 1;
            }
            if (dateDayNow < dayDateAgeUser && dateMonthNow == monthDateAgeUser) {
                return dateYearNow - this.birthday - 1;
            }
                return dateYearNow - this.birthday;

        },
        userPassword: password,
        get getPassword() {
             return (this.firstName[0]).toUpperCase() + (this.surname).toLowerCase() + yearDateAgeUser;
        }

    }
}

function readField(message) {
    return prompt(message);
}

function readBDay() {
    do {
        let dateUser = prompt("Enter date of birth");
        if (dateUser.includes(",")){
            alert("Your dater is not currect - dd.mm.yyyy");
            continue;
        }
        let ourDate = dateUser.split(".");
        let currentYear = new Date().getFullYear();
        let day = +ourDate[0];
        if (day < 1 || day > 31) {
            alert("Date of birth is not currect - dd.mm.yyyy");
            continue;
        }
        let month = +ourDate[1];
        month--;
        if (month < 1 || month > 12) {
            alert("Date of birth is not currect - dd.mm.yyyy");
            continue;
        }


        let year = +ourDate[2];
        if (year < 1900 || year > currentYear) {
            alert("Date of birth is not currect - dd.mm.yyyy");
            continue;
        }

        return arrayUserDate =[year, month, day]

    } while (true)
}

console.log(createNewUser());
console.log(newUser.getAge);
console.log(newUser.getPassword);
